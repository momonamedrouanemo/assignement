package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.exceptions.UtilisateurNonExistantException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController(value = "/versement")
public class VeresementController {

    public static final int MONTANT_MAXIMAL = 10000;


    @Autowired
    private VersementRepository versementRepository;

    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    private AuditService auditService;



    @GetMapping("lister-versement")
    List<Versement> getVersements() {
        List<Versement> versementList = versementRepository.findAll();

        if (CollectionUtils.isEmpty(versementList)) {
            return null;
        } else {
            return versementList;
        }
    }


    @PostMapping("/create-versement")
    @ResponseStatus(HttpStatus.CREATED)
    public void makeVersement(@RequestBody VersementDto versementDto)
            throws TransactionException, CompteNonExistantException, UtilisateurNonExistantException {

        Compte compte = compteRepository.findCompteByrib(versementDto.getRib());


        if(compte == null){
            throw new CompteNonExistantException("Compte Non existant");
        }
        if (versementDto.getNomPrenomEmetteur() == null){
            throw new UtilisateurNonExistantException("Utilisateur non existant");
        }
        if (versementDto.getMontantVersement().equals(null)) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (versementDto.getMontantVersement().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }
        if (versementDto.getMotifVersement().length() < 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        compte.setSolde(
                new BigDecimal(compte.getSolde().intValue() +
                        versementDto.getMontantVersement().intValue()));

        Versement versement = VersementMapper.dto2VersementMapper(versementDto, compteRepository);
        versementRepository.save(versement);

        auditService.auditVersement("Versement depuis " + versementDto.getNomPrenomEmetteur() + " vers " + versementDto
                .getRib() + " d'un montant de " + versementDto.getMontantVersement()
                .toString());



    }


}
