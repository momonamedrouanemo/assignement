package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController(value = "comptes")
public class CompteController {
    @Autowired
    private CompteRepository compteRepository;

    @GetMapping("lister-compte")
    List<Compte> getComptes(){
        List<Compte> compteList = compteRepository.findAll();

        if (CollectionUtils.isEmpty(compteList)) {
            return null;
        } else {
            return compteList;
        }
    }




}
