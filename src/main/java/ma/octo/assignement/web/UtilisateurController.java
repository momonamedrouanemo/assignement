package ma.octo.assignement.web;


import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController(value = "users")
public class UtilisateurController {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @GetMapping("lister-utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll();

        if (CollectionUtils.isEmpty(utilisateurList)) {
            return null;
        } else {
            return utilisateurList;
        }
    }
}
