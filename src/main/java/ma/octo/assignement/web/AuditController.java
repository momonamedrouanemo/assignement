package ma.octo.assignement.web;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.repository.AuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController(value = "audit-service")
public class AuditController {

    @Autowired
    private AuditRepository auditRepository;

    @GetMapping("audits")
    List<Audit> getAudits(){
        List<Audit> auditList = auditRepository.findAll();
        if(CollectionUtils.isEmpty(auditList)){
            return null;
        }
        return auditList;
    }
}
