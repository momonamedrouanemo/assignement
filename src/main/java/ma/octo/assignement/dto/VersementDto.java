package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

public class VersementDto {
    private String rib;
    private String nomPrenomEmetteur;
    private String motifVersement;
    private BigDecimal montantVersement;
    private Date dateExecution;

    public String getRib() {
        return rib;
    }

    public void setRib(String nrCompteEmetteur) {
        this.rib = nrCompteEmetteur;
    }


    public BigDecimal getMontantVersement() {
        return montantVersement;
    }

    public void setMontantVersement(BigDecimal montantVersement) {
        this.montantVersement = montantVersement;
    }

    public String getMotifVersement() {
        return motifVersement;
    }

    public void setMotifVersement(String motifVersement) {
        this.motifVersement = motifVersement;
    }

    public Date getDateExecution() {
        return dateExecution;
    }

    public void setDateExecution(Date dateExecution) {
        this.dateExecution = dateExecution;
    }

    public String getNomPrenomEmetteur() {
        return nomPrenomEmetteur;
    }

    public void setNomPrenomEmetteur(String nomPrenomEmetteur) {
        this.nomPrenomEmetteur = nomPrenomEmetteur;
    }

}
