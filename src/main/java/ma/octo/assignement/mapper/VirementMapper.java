package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class VirementMapper {


    public static Virement dto2VirementMapper(VirementDto virementDto, CompteRepository compteRepository){

        Virement virement = new Virement();
        virement.setMontantVirement(virementDto.getMontantVirement());
        virement.setCompteBeneficiaire(compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire()));
        virement.setCompteEmetteur(compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur()));
        virement.setMotifVirement(virementDto.getMotif());
        virement.setDateExecution(virementDto.getDate());


        return virement;

    }
}
