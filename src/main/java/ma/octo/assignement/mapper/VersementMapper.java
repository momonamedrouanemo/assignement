package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.repository.CompteRepository;


public class VersementMapper {

    public static Versement dto2VersementMapper(VersementDto versementDto, CompteRepository compteRepository){
        Versement versement = new Versement();
        versement.setNomPrenomEmetteur(versementDto.getNomPrenomEmetteur());
        versement.setCompteBeneficiaire(compteRepository.findCompteByrib(versementDto.getRib()));
        versement.setMontantVersement(versementDto.getMontantVersement());
        versement.setMotifVersement(versementDto.getMotifVersement());
        versement.setDateExecution(versementDto.getDateExecution());

        return versement;
    }
}
