package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class VersementMapperTest {

    @Autowired
    VersementRepository versementRepository;

    @Autowired
    CompteRepository compteRepository;

    @Test
    void dto2VersementMapper() {
        VersementDto versementDto = new VersementDto();

        versementDto.setNomPrenomEmetteur("ROUANE");
        versementDto.setRib("RIB1");
        versementDto.setMontantVersement(new BigDecimal(1000));
        versementDto.setMotifVersement("Versement Test");
        versementDto.setDateExecution(new Date());

        Versement versement = VersementMapper.dto2VersementMapper(versementDto, compteRepository);

        // test the validation of some fields
        assertEquals(versementDto.getRib(),
                versement.getCompteBeneficiaire().getRib());
        assertEquals(versementDto.getDateExecution(),
                versement.getDateExecution());


    }
}