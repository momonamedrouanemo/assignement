package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class VirementMapperTest {

    @Autowired
    VirementRepository virementRepository;
    @Autowired
    CompteRepository compteRepository;

    @Test
    void dto2VirementMapper() {

        VirementDto virementDto = new VirementDto();

        virementDto.setMontantVirement(new BigDecimal(1000));
        virementDto.setMotif("Virement test");
        virementDto.setDate(new Date());
        virementDto.setNrCompteBeneficiaire("010000A000001000");
        virementDto.setNrCompteBeneficiaire("010000B025001000");

        Virement virement = VirementMapper.dto2VirementMapper(virementDto, compteRepository);

        // test some fields
        assertEquals(virementDto.getMontantVirement(),
                virement.getMontantVirement());
        assertEquals(virementDto.getNrCompteBeneficiaire(),
                virement.getCompteBeneficiaire().getNrCompte());

    }
}